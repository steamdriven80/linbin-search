
import random, os
import subprocess

FieldSizes = [1000, 10000, 100000, 1000000]
QuerySize = 100

for FieldSize in FieldSizes:

	output_string = str(FieldSize)+" "+str(QuerySize)+' '

	Fields = []
	Querys = []

	for x in xrange(0, FieldSize):
		Fields.append( str(random.randint(-1000000, 1000000)) )

	output_string = output_string + ' '.join(Fields)+' '
	for x in xrange(0, QuerySize):
		val = Fields[random.randint(0,len(Fields) - 1)]

		if random.random() > 0.7:
			val = random.randint(-1000000, 1000000)

		Querys.append( str(val) )

	output_string = output_string + ' '.join(Querys)

	with open('tmp.file', 'wb') as out:
		out.write(output_string)

	subprocess.call('<tmp.file ./searcher', shell=True) 

	os.remove('tmp.file')